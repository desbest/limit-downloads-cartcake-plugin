# Limit Downloads plugin for Cartcake

Compatible with Cartcake v1.2

This plugin for Cartcake allows you to limit the amount of times that files can be downloaded for digital products that users purchase.
This can be used to encourage that users share the files with each other or prevent piracy.

* [more information on Cartcake](http://desbest.com/cartcake) 
* [git repository](http://gitlab.com/desbest/cartcake)
* [created by desbest](http://gitlab.com)
