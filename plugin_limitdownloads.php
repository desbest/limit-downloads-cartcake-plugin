<?php
    /*
    About   */    
    $pl_Title = "Limit Downloads";
    $pl_Description = "Make it so that people can only download products X amount of times.";
    $pl_Version = "1"; $pl_Compatibility = "1.2"; 
    $pl_Hook = "beforeDownload/afterDownload"; $pl_Filename = "plugin_limitdownloads";
    $pl_Author = "desbest"; $pl_Website = "http://desbest.uk.to"; $pl_Twitter = "tynamite"; $pl_Date = "16/01/2012";
    ////
    
    /*
    Installation   */    
    $ph_Settings[0] = "maxdownloads";
    
    ////
    
    /*
    Activity   */
    if (isset($hook_BeforeDownload) && $hook_BeforeDownload == "yes"){
    
        $pluginid = mysql_query("SELECT id FROM plugins WHERE file='$pl_Filename' ");
        $pluginid = mysql_result($pluginid, 0);
        
        $fetchdownloads = mysql_query("SELECT * FROM pluginstorage WHERE pluginid='$pluginid' && xtable='ordered' && xid='$orderedid' && attribute='downloads' ");
        $countdownloads = mysql_num_rows($fetchdownloads);
        if ($countdownloads >0) { $download = mysql_fetch_array($fetchdownloads); $dltimes = $download[value]; }
        if ((empty($dltimes))){ $dltimes = "0"; }
        
        $fetchsettings = mysql_query("SELECT value FROM pluginsettings WHERE attribute='maxdownloads' ");
        $clickfingers = mysql_result($fetchsettings, 0);
        if ($clickfingers <= $download[value]) { die("You have exceeded your download attempts."); } else { $result = "can download"; }
        
        /* //debug mode
        $myFile = "functions/test.txt"; $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, "testing shiz"."\n"); fwrite($fh, $clickfingers."\n"); fwrite($fh, $dltimes."\n"); fwrite($fh, $result."\n");
        fclose($fh);   */     
    }
    
    if (isset($hook_AfterDownload) && $hook_AfterDownload == "yes"){
        $pluginid = mysql_query("SELECT id FROM plugins WHERE file='$pl_Filename' ");
        $pluginid = mysql_result($pluginid, 0);
        
        $fetchdownloads = mysql_query("SELECT * FROM pluginstorage WHERE pluginid='$pluginid' && xtable='ordered' && xid='$orderedid' && attribute='downloadcount' ");
        $countdownloads = mysql_num_rows($fetchdownloads); $download = mysql_fetch_array($fetchdownloads);
        
        //debug mode
        //$myFile = "functions/test.txt"; $fh = fopen($myFile, 'w') or die("can't open file");
        //fwrite($fh, "testing shiz"."\n"); fwrite($fh, $pluginid."\n"); fwrite($fh, $orderedid."\n"); fwrite($fh, $countdownloads."\n");
        //fclose($fh);   
        
        if ($countdownloads == 0){
            mysql_query("INSERT INTO pluginstorage 
            (pluginid, xtable, xid, attribute, value) 
            VALUES('$pluginid', 'ordered', '$orderedid', 'downloadcount', '1' ) ") 
            or die(mysql_error());  
        } else {
            $newValue = $download[value] +1;
            $shake = mysql_query("UPDATE pluginstorage SET value='$newValue' WHERE pluginid='$pluginid' && xtable='ordered' && xid='$orderedid' && attribute='downloadcount' ") or die (mysql_error());
        }
    }
    
    ////


?>